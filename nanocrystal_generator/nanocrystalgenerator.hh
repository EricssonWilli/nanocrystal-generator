#ifndef NANOCRYSTALGENERATOR_H
#define NANOCRYSTALGENERATOR_H
#include <string>
#include "atom.hh"
#include <vector>
#include <unordered_map>
#include <random>
#include <memory>
#include <Eigen/Geometry>

enum LatticeType {
    NO_TYPE, BCC, FCC, HCP, CUSTOM
};

const std::unordered_map<std::string, LatticeType> TYPE_STRUCT =
{
    {"bcc", LatticeType::BCC},
    {"fcc", LatticeType::FCC},
    {"hcp", LatticeType::HCP},
    {"custom", LatticeType::CUSTOM}
};
struct LmpAtom{
    int type=0;
    Eigen::Vector3d coordinates={0,0,0};
};
struct CustomLatticeDefinition{
    std::vector<std::string> masses={};
    std::vector<LmpAtom> atoms={};
    Eigen::Vector3d shift_values={0,0,0};
};

class NanocrystalGenerator
{
public:
    NanocrystalGenerator(int argc, char** argv);
    ~NanocrystalGenerator();
    /**
     * @brief greet prints the greeting message to cout
     */
    void greet();
    /**
     * @brief save_lammps saves the generated atoms in a lammps data formatted file
     */
    void save_lammps();
    /**
     * @brief generate_polycrystal generates the atoms with the selected parameters to the RAM
     */
    void generate_polycrystal();
    /**
     * @brief is_inside_pillar_perimeters checks if the coordinates are within the to be generated pillar
     * @param coordinates coordinates to be checked
     * @return boolean value true if the coordinates are within the pillar
     */
    bool is_inside_pillar_perimeters(const Eigen::Vector3d& coordinates);
    /**
     * @brief read_atomsk_param_file reads the atomsk parameter file that include some of the input parameters
     * @param file_name the file to be read
     */
    void read_atomsk_param_file(const std::string& file_name);
    /**
     * @brief randomize_grains randomizes the grains (orientation and location)
     */
    void randomize_grains();
    /**
     * @brief save_random saves the random configuration to a file
     */
    void save_random();
    /**
     * @brief save_params saves an Atomsk compatible params file
     */
    void save_params();
    /**
     * @brief save_grain_statistics saves statistics from the grain configuration of the bulk material
     */
    void save_grain_statistics();

private:
    /**
     * @brief read_lattice, reads a lmp file to use as the custom lattice unit
     * @param filename as the name suggests, the name of the file
     * @return boolean, true if the reading was successful
     */
    bool read_lattice(const std::string & filename);
    std::string output_file_name;
    LatticeType lattice_type;
    std::vector<Atom> atoms;
    std::vector<std::string> atom_types;
    double lattice_constant;
    double diameter;
    Eigen::AlignedBox3d box;
    double tan_value;
    std::vector<Grain> grains;
    int grain_amount;
    bool periodic;
    bool shrink_wrap;
    long long int seed;
    std::string atom_type;
    bool triclinic;
    bool wrapping;
    bool grain_statistics;
    CustomLatticeDefinition custom;
    double height;

};

#endif // NANOCRYSTALGENERATOR_H
