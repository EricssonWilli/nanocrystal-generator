#ifndef ATOM_HH
#define ATOM_HH

#include <string>
#include <Eigen/Dense>
#include "grain.hh"



struct Atom {
    Eigen::Vector3d coordinates = {0.0, 0.0, 0.0};
    int type = 0;
};

#endif // ATOM_HH
