#ifndef GRAIN_HH
#define GRAIN_HH

#include <Eigen/Dense>
namespace Constants{
const int NO_GRAIN = -1;
const int NO_ID = -1;
const double NO_VOLUME = -1;
const std::string NO_NAME = "NO";
const Eigen::Vector3d NO_POSITION = {-1,-1,-1};
const Eigen::Vector3d NO_ANGLE = {0,0,0};
}

struct Grain{
    int grain_id = Constants::NO_GRAIN;
    Eigen::Vector3d position = Constants::NO_POSITION;
    Eigen::Vector3d euler_angle = Constants::NO_ANGLE;
    unsigned int atom_count = 0;
    double volume = Constants::NO_VOLUME;
    double estim_volume = Constants::NO_VOLUME;
    Eigen::Vector3d mass_center_point = Constants::NO_POSITION;
};

#endif // GRAIN_HH
