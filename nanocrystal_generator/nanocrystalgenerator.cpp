#include "nanocrystalgenerator.hh"
#include "CLI11.hpp"
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <Eigen/Dense>
#include "grain.hh"
#include <random>
#include <cmath>
#include <Eigen/Geometry>
#include "atommasses.hh"
#include <voropp/voro++.hh>

NanocrystalGenerator::NanocrystalGenerator(int argc, char **argv):
    output_file_name("file.lmp"), lattice_type(NO_TYPE), atoms({}), atom_types({}), lattice_constant(-1.0), diameter(std::numeric_limits<double>::max()),
    box(Eigen::Vector3d{0,0,0},Eigen::Vector3d{0,0,0}), tan_value(tan(2.5*M_PI/180)), grains({}), grain_amount(1), periodic(true), shrink_wrap(false),
    seed(0), atom_type("XX"), triclinic(false), wrapping(true), grain_statistics(false), height(std::numeric_limits<double>::max())
{
    CLI::App app{"Nanocrystal Generator"};

    std::string lattice_string = "";
    std::string file_name = "";
    std::vector<double> perimeter_vector = {};

    // required parameters
    app.add_option("-f,--filename", output_file_name, "Output file name")->required();
    app.add_option("-l,--lattice_type", lattice_string, "Lattice type to generate, or \"custom\"")->required();

    app.add_option("-t,--atom_type",atom_type,"Atom type to be generated or a lammps file for lattice definition")->required();

    // required if not custom lattice type
    app.add_option("-c,--lattice_constant", lattice_constant, "Lattice constant, whatever numeric value if using custom lattice");
    // optional parameters
    app.add_option("-a,--atomsk",file_name,"File name of input atomsk -compatible parameter file");

    app.add_option("-b,--box", perimeter_vector, "Box perimeters in Å")->expected(3);
    app.add_option("--seed",seed,
                   "Seed for random number generation (no parameter or 0 -> time as seed)");
    app.add_option("-g,--grain_amount", grain_amount, "Grain amount");
    // pillar related options
    app.add_option("-d,--diameter",diameter, "Diameter in nm");
    app.add_option("--height", height, "Height in nm");
    double angle = 2.5;
    app.add_option("--angle", angle, "Taper angle in degrees");

    app.add_flag("-p,!-n",periodic,"Do not generate using periodic boundary conditions");
    app.add_flag("-s,!--no_shrink_wrap",shrink_wrap,
                   "\"Shrink wrap\" the boundary box (make it as small as possible by keeping all the atoms inside it)");
    app.add_flag("--triclinic,!--orthogonal",triclinic, "Use triclinic box");
    app.add_flag("!--no_wrap", wrapping,"Do not wrap periodic grains back inside the box");
    app.add_flag("--grain_statistics", grain_statistics,"Generate grain statistics file");
    try {
        app.parse(argc, argv);
    } catch( const CLI::ParseError & e){
        app.exit(e);
        throw e;
    }
    if(ATOM_TO_MASS.find(atom_type) == ATOM_TO_MASS.end()){
        if (TYPE_STRUCT.find(lattice_string) == TYPE_STRUCT.end() || TYPE_STRUCT.at(lattice_string) != LatticeType::CUSTOM){
            std::cout << "Error: Unrecognized atom type, exiting" << std::endl;
            return;
        }

        if(!read_lattice(atom_type)){
            std::cout << "Error: File missing or invalid format" << std::endl;
            return;
        }

    }
    lattice_type = TYPE_STRUCT.at(lattice_string);
    if ((lattice_type != LatticeType::CUSTOM) && (lattice_constant < 0)) {
        std::cout << "Error: Missing lattice constant" << std::endl;
        return;
    }
    // tan value update
    tan_value = tan(angle*M_PI/180);
    greet();
    std::cout << "Input parameters given to the program: " << std::endl;

    for( int i=1; i<argc; ++i){
        std::cout << argv[i] << ' ';
    }
    std::cout << std::endl;

    if (file_name != "") {
        read_atomsk_param_file(file_name);
        grain_amount = grains.size();
    } else {
        box=Eigen::AlignedBox3d(Eigen::Vector3d{0,0,0},Eigen::Vector3d{perimeter_vector[0],perimeter_vector[1],perimeter_vector[2]});
        randomize_grains();
        save_random();
        save_params();
    }



    if (lattice_type != LatticeType::CUSTOM) {
        atom_types.push_back(atom_type);
    }


    generate_polycrystal();
    save_lammps();
    if (grain_statistics) {
        save_grain_statistics();
    }

}

NanocrystalGenerator::~NanocrystalGenerator()
{

}

void NanocrystalGenerator::greet()
{
    std::cout << "Nanocrystal Generator script by Eerik Voimanen (c) 2022 version 2" <<std::endl;
}

void NanocrystalGenerator::save_lammps()
{
    std::cout << "Saving to a file" << std::endl;
    auto box_min = box.min();
    auto box_max = box.max();
    std::ofstream output_stream(output_file_name, std::ios_base::out);
    std::string type = grain_amount==1 ? "Mono" : "Poly";
    output_stream << "# " << type <<"crystal file generated with Nanocrystal Generator" << std::endl << std::endl;
    output_stream << std::right << std::setw(12) << atoms.size();
    output_stream << "  " << "atoms\n";
    std::string atom_type_string = "1";
    if (lattice_type == LatticeType::CUSTOM) {
        atom_type_string = std::to_string(custom.masses.size());
    }
    output_stream << std::right << std::setw(12) << atom_type_string;
    output_stream << "  " << "atom types\n\n";
    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[0] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[0] << "  " << "xlo xhi\n";
    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[1] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[1] << "  " << "ylo yhi\n";
    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[2] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[2] << "  " << "zlo zhi\n";
    if (triclinic){
        output_stream << "      0.0000000    0.00000000    0.00000000  xy xz yz\n";
    }
    output_stream << "\n";
    output_stream << "Masses\n\n";
    int i = 1;
    if (lattice_type != LatticeType::CUSTOM){
        for (auto type : atom_types){
            output_stream << std::right << std::setw(13) << i << std::setw(15) << std::fixed
                          << std::setprecision(8) << ATOM_TO_MASS.at(type) << "            # " << type<< "\n\n";
            ++i;
        }
    } else {
        for (auto& line : custom.masses){
            output_stream << line << '\n';
        }
        output_stream << '\n';
    }
    output_stream << "Atoms\n\n";
    i = 0;
    std::for_each(atoms.begin(),atoms.end(), [&i, &output_stream](auto && atom){
        output_stream << std::right << std::setw(10) << ++i << std::setw(5) << atom.type
                      << std::fixed << std::setprecision(12) << std::setw(22) << atom.coordinates[0]
                      << " "
                      << std::fixed << std::setprecision(12) << std::setw(22)
                      << atom.coordinates[1] << " " << std::fixed << std::setprecision(12) << std::setw(22) << atom.coordinates[2] << "\n";
    });
    output_stream.close();
    std::cout << "Wrote to a file " << output_file_name << " successfully." << std::endl;
}

void NanocrystalGenerator::generate_polycrystal()
{
    auto box_max = box.max();
    std::vector<Eigen::Vector3d> generated_offsets ={};
    switch (lattice_type) {
    case BCC:
        generated_offsets = {{0.5,0.5,0.5},{0,0,0}};
        break;
    case FCC:
        generated_offsets = {{0,0,0},{0.5,0.5,0},{0,0.5,0.5},{0.5,0,0.5}};
        break;
    case HCP:
        generated_offsets = {{1/sqrt(3),0,0.5*sqrt(8/3)},{0,0,0}};
        break;
    default:
        break;
    }


    int atom_count = std::ceil(box_max[0]/lattice_constant) *
            std::ceil(box_max[1]/lattice_constant) *
            std::ceil(box_max[2]/lattice_constant) * generated_offsets.size();
    atoms.reserve(atom_count);

    voro::container con(0,box_max[0],0,box_max[1],0,box_max[2],1,1,1,periodic,periodic,periodic,grain_amount);
    std::for_each(grains.begin(),grains.end(),[&con](auto& grain){con.put(grain.grain_id, grain.position[0],grain.position[1],grain.position[2]);});

    voro::particle_order po;
    voro::c_loop_all clo(con);
    voro::voronoicell c;
    double x,y,z;
    auto grain = grains.begin();
    if(clo.start()) do if(con.compute_cell(c,clo)) {
        clo.pos(x,y,z);
        std::vector<double> v;
        c.vertices(v);
        std::vector<Eigen::Vector3d> coordinates = {};
        for (std::vector<double>::size_type i = 0;i<v.size();i+=3) {
            coordinates.push_back({v[i],v[i+1],v[i+2]});
        }
        int grain_id = clo.pid();
        std::cout << "Generating grain "<< grain_id << std::endl;

        Eigen::AngleAxisd alphaAngle(grain->euler_angle[0]*2*M_PI/360, Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd betaAngle(grain->euler_angle[1]*2*M_PI/360, Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd gammaAngle(grain->euler_angle[2]*2*M_PI/360, Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> q = alphaAngle * betaAngle * gammaAngle;

        Eigen::Matrix3d rotation_matrix = q.matrix();

        Eigen::AngleAxisd invalphaAngle(-grain->euler_angle[0]*2*M_PI/360, Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd invbetaAngle(-grain->euler_angle[1]*2*M_PI/360, Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd invgammaAngle(-grain->euler_angle[2]*2*M_PI/360, Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> invq = invgammaAngle * invbetaAngle * invalphaAngle;

        Eigen::Matrix3d inv_rotation_matrix = invq.matrix();

        for (auto& entry : coordinates){
            entry = inv_rotation_matrix * entry;
        }
        double min_x = (*std::min_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[0]<b[0];}))[0];
        double max_x = (*std::max_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[0]<b[0];}))[0];
        double min_y = (*std::min_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[1]<b[1];}))[1];
        double max_y = (*std::max_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[1]<b[1];}))[1];
        double min_z = (*std::min_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[2]<b[2];}))[2];
        double max_z = (*std::max_element(coordinates.begin(),coordinates.end(),[](auto& a, auto& b){return a[2]<b[2];}))[2];

        std::cout << "Creating atoms" <<  std::endl;
        std::vector<Atom> tmp_atoms ={};
        // TODO duplicate code could be removed unless it results in major peformance decrease
        if (lattice_type == HCP) {
            for (auto& offset : generated_offsets) {
                Eigen::Vector3d absolute_offset = offset*lattice_constant;
                for (Eigen::Vector3d i = absolute_offset; i[0]<((max_x-min_x)+sqrt(3)/2*lattice_constant) ; i[0]+=sqrt(3)/2*lattice_constant,i[1]-=0.5*lattice_constant) {
                    if (i[1] <= -lattice_constant) {
                        i[1]+=lattice_constant;
                    }
                    for (Eigen::Vector3d j = i; j[1]<((max_y-min_y)+lattice_constant); j[1]+=lattice_constant) {
                        int max_index = ((max_z-min_z) / lattice_constant /sqrt(8/3))+1;
                        #pragma omp parallel
                        {
                            std::vector<Atom> per_thread = {};
                            Eigen::Vector3d coordinates;
                            #pragma omp for
                            for (int i= 0; i<max_index; ++i) {
                                coordinates = rotation_matrix * ((j+Eigen::Vector3d{0,0,lattice_constant*sqrt(8/3)}*i)+Eigen::Vector3d(min_x,min_y,min_z)) + Eigen::Vector3d(x,y,z);

                                double testx = 0;
                                double testy = 0;
                                double testz = 0;
                                int testpid = -1;
                                Eigen::Vector3d wrapped_coords(std::fmod(coordinates[0]+box_max[0], box_max[0]),std::fmod(coordinates[1]+box_max[1], box_max[1]),std::fmod(coordinates[2]+box_max[2], box_max[2]));
                                if (is_inside_pillar_perimeters(wrapped_coords)){
                                    con.find_voronoi_cell(coordinates[0],coordinates[1],coordinates[2],testx,testy,testz,testpid);
                                    if ((testpid == grain_id) && (x == testx) && (y == testy) && (z == testz)){
                                        if (wrapping){
                                            per_thread.push_back(Atom{wrapped_coords,1});
                                        } else {
                                            per_thread.push_back(Atom{coordinates,1});
                                        }
                                    }
                                }

                            }
                            #pragma omp critical
                            {
                                tmp_atoms.insert(tmp_atoms.end(),per_thread.begin(),per_thread.end());
                            }
                        }
                    }
                }

            }

        } else if (lattice_type == LatticeType::CUSTOM){

            for (auto& customAtom : custom.atoms) {
                Eigen::Vector3d offset = customAtom.coordinates;
                const int atom_type = customAtom.type;
                Eigen::Vector3d absolute_offset = offset*lattice_constant;
                for (Eigen::Vector3d i = absolute_offset; i[0]<((max_x-min_x)+custom.shift_values[0]) ; i[0]+=custom.shift_values[0]) {

                    for (Eigen::Vector3d j = i; j[1]<((max_y-min_y)+custom.shift_values[1]); j[1]+=custom.shift_values[1]) {
                        int max_index = ((max_z-min_z) / custom.shift_values[2])+1;
                        #pragma omp parallel
                        {
                            std::vector<Atom> per_thread = {};
                            Eigen::Vector3d coordinates;
                            #pragma omp for
                            for (int i= 0; i<max_index; ++i) {
                                coordinates = rotation_matrix * ((j+Eigen::Vector3d{0,0,custom.shift_values[2]}*i)+Eigen::Vector3d(min_x,min_y,min_z)) + Eigen::Vector3d(x,y,z);

                                double testx = 0;
                                double testy = 0;
                                double testz = 0;
                                int testpid = -1;
                                Eigen::Vector3d wrapped_coords(std::fmod(coordinates[0]+box_max[0], box_max[0]),std::fmod(coordinates[1]+box_max[1], box_max[1]),std::fmod(coordinates[2]+box_max[2], box_max[2]));
                                if (is_inside_pillar_perimeters(wrapped_coords)){
                                    con.find_voronoi_cell(coordinates[0],coordinates[1],coordinates[2],testx,testy,testz,testpid);
                                    if ((testpid == grain_id) && (x == testx) && (y == testy) && (z == testz)){
                                        if (wrapping){
                                            per_thread.push_back(Atom{wrapped_coords,atom_type});
                                        } else {
                                            per_thread.push_back(Atom{coordinates,atom_type});
                                        }
                                    }
                                }

                            }
                            #pragma omp critical
                            {
                                tmp_atoms.insert(tmp_atoms.end(),per_thread.begin(),per_thread.end());
                            }
                        }
                    }
                }

            }
        } else {

            for (auto& offset : generated_offsets) {
                Eigen::Vector3d absolute_offset = offset*lattice_constant;
                for (Eigen::Vector3d i = absolute_offset; i[0]<((max_x-min_x)+lattice_constant) ; i[0]+=lattice_constant) {
                    for (Eigen::Vector3d j = i; j[1]<((max_y-min_y)+lattice_constant); j[1]+=lattice_constant) {
                        int max_index = ((max_z-min_z) / lattice_constant)+1;
                        #pragma omp parallel
                        {
                            std::vector<Atom> per_thread = {};
                            Eigen::Vector3d coordinates;
                            #pragma omp for
                            for (int i= 0; i<max_index; ++i) {
                                coordinates = rotation_matrix * ((j+Eigen::Vector3d{0,0,lattice_constant}*i)+Eigen::Vector3d(min_x,min_y,min_z)) + Eigen::Vector3d(x,y,z);

                                double testx = 0;
                                double testy = 0;
                                double testz = 0;
                                int testpid = -1;
                                Eigen::Vector3d wrapped_coords(std::fmod(coordinates[0]+box_max[0], box_max[0]),std::fmod(coordinates[1]+box_max[1], box_max[1]),std::fmod(coordinates[2]+box_max[2], box_max[2]));
                                if (is_inside_pillar_perimeters(wrapped_coords)){
                                    con.find_voronoi_cell(coordinates[0],coordinates[1],coordinates[2],testx,testy,testz,testpid);
                                    if ((testpid == grain_id) && (x == testx) && (y == testy) && (z == testz)){
                                        if (wrapping){
                                            per_thread.push_back(Atom{wrapped_coords,1});
                                        } else {
                                            per_thread.push_back(Atom{coordinates,1});
                                        }
                                    }
                                }

                            }
                            #pragma omp critical
                            {
                                tmp_atoms.insert(tmp_atoms.end(),per_thread.begin(),per_thread.end());
                            }
                        }

                    }
                }
            }
        }
        atoms.reserve(atoms.size()+(tmp_atoms.end()-tmp_atoms.begin()));
        if (grain_statistics) {
            Eigen::Vector3d mass_center_point = {0,0,0};
            for (auto atom = tmp_atoms.begin(); atom != tmp_atoms.end(); ++atom) {
                mass_center_point+=atom->coordinates;
            }
            grain->mass_center_point = mass_center_point/tmp_atoms.size();
        }
        std::cout << "The number of generated atoms (this grain): " << tmp_atoms.size() << std::endl;
        grain->volume = c.volume();
        grain->atom_count = tmp_atoms.size();

        atoms.insert(atoms.end(),tmp_atoms.begin(),tmp_atoms.end());
        ++grain;

    } while (clo.inc());

    unsigned int all_atom_count = atoms.size();
    double box_volume = box.volume();

    for (auto grain = grains.begin(); grain !=grains.end(); ++grain) {
        grain->estim_volume = grain->atom_count*box_volume/all_atom_count;
    }

    if (shrink_wrap) {
        std::cout << "Shrink wrapping" << std::endl;
        Eigen::Vector3d minimum = {0,0,0};
        #pragma omp parallel for
        for (int i = 0; i < 3; ++i){
            minimum[i] = (*std::min_element(atoms.begin(),atoms.end(),[&i](auto && atom1, auto && atom2){return atom1.coordinates[i] < atom2.coordinates[i];})).coordinates[i];
        }
        #pragma omp parallel for
        for(auto iter = atoms.begin(); iter<=  atoms.end();++iter){
            iter->coordinates -=minimum;
        }

        Eigen::Vector3d maximum = {0,0,0};
        #pragma omp parallel for
        for (int i = 0; i < 3; ++i) {
            maximum[i] = (*std::max_element(atoms.begin(),atoms.end(),[&i](auto && atom1, auto && atom2){return atom1.coordinates[i] < atom2.coordinates[i];})).coordinates[i];
        }

        box=Eigen::AlignedBox3d{Eigen::Vector3d{0,0,0},maximum};

    }
    std::cout << "Grains generated" << std::endl;
}

bool NanocrystalGenerator::is_inside_pillar_perimeters(const Eigen::Vector3d& coordinates)
{
    Eigen::Vector3d halfway = box.max()*0.5;
    halfway[2]=0;
    if (coordinates[2]>10*height)
        return false;
    if (std::sqrt(std::pow(halfway[0]-coordinates[0],2)+std::pow(halfway[1]-coordinates[1],2))>5*diameter + (5*height-coordinates[2])*tan_value)
        return false;
    return true;
}

void NanocrystalGenerator::read_atomsk_param_file(const std::string &file_name)
{
    //std::vector<Grain> voronoi_points = {};
    std::cout << "Reading atomsk parameter file from " << file_name << std::endl;
    std::ifstream input_stream(file_name, std::ios_base::in);

    std::string line = "";
    while (std::getline(input_stream, line)){
        if(line.empty() || line[0] == '#'){
            continue;
        }
        std::stringstream s(line);
        std::string command = "";
        s >> command;
        if (command == "box") {
            Eigen::Vector3d min{0,0,0};
            Eigen::Vector3d max{0,0,0};
            for (int i = 0; i<3; ++i){
                s >> max[i];

            }
            box = {min,max};
        }else if (command == "node"){
            Eigen::Vector3d pos = {0,0,0};
            Eigen::Vector3d angles = {0,0,0};
            for (int i = 0; i<3; ++i) {
                s >> pos[i];
            }
            for (int i = 0; i<3; ++i) {
                s >> angles[i];
            }
            Grain grain{(int)grains.size()+1,pos,angles};
            grains.push_back(grain);
        } else {
        }
    }
    input_stream.close();
    std::cout << "Reading complete:\n" << "Box dimensions X " << box.max()[0] << " Y " <<  box.max()[1] << " Z " << box.max()[2] << std::endl;
    std::cout << "Grain amount " << grains.size() << std::endl;

}

void NanocrystalGenerator::randomize_grains()
{
    if (seed == 0) {
        seed = time(0);
    }
    // Initialize random number generator with a seed (chosen or time)
    std::mt19937 gen(seed);
    // Initialize all the rng-functions for all of the necessary parts
    std::uniform_real_distribution<> rng_x(box.min()[0],box.max()[0]);
    std::uniform_real_distribution<> rng_y(box.min()[1],box.max()[1]);
    std::uniform_real_distribution<> rng_z(box.min()[2],box.max()[2]);
    std::uniform_real_distribution<> rng_angle(0,360);
    std::uniform_real_distribution<> rng_to_arccos(-1,1);



    // reserve and randomize the grains
    grains.reserve(grain_amount);
    for (int i = 1;i <= grain_amount ;++i ) {
        grains.push_back(
                    Grain{i,Eigen::Vector3d{(rng_x(gen)), rng_y(gen), rng_z(gen)},
                          Eigen::Vector3d{rng_angle(gen),acos(rng_to_arccos(gen))*180/M_PI,rng_angle(gen)}});
    }
}

void NanocrystalGenerator::save_random()
{
    std::cout << "Saving grains to a file" << std::endl;
    auto box_min = box.min();
    auto box_max = box.max();
    std::ofstream output_stream(output_file_name+".voronoi", std::ios_base::out);
    output_stream << "# Voronoi positions" << std::endl << std::endl;
    output_stream << std::right << std::setw(12) << grains.size();
    output_stream << "  " << "atoms\n";
    output_stream << std::right << std::setw(12) << "1";
    output_stream << "  " << "atom types\n\n";

    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[0] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[0] << "  " << "xlo xhi\n";
    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[1] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[1] << "  " << "ylo yhi\n";
    output_stream << std::right << std::fixed << std::setw(20) <<
                     std::setprecision(12) << box_min[2] << " "
                  << std::setw(20) << std::setprecision(12) <<
                     box_max[2] << "  " << "zlo zhi\n\n";

    output_stream << "Masses\n\n";
    if (lattice_type != LatticeType::CUSTOM){
        output_stream << std::right << std::setw(13) << "1" << std::setw(15) << std::fixed << std::setprecision(8) << ATOM_TO_MASS.at(atom_type)<< "\n\n";
    } else {

        output_stream << std::right << std::setw(13) << "1" << std::setw(15) << std::fixed << std::setprecision(8) << ATOM_TO_MASS.at("H")<< "\n\n";

    }
    output_stream << "Atoms\n\n";
    int i = 0;
    std::for_each(grains.begin(),grains.end(), [&i, &output_stream](auto && atom){
        output_stream << std::right << std::setw(10) << ++i << std::setw(5) << "1"
                      << std::fixed << std::setprecision(12) << std::setw(22) << atom.position[0]
                      << " "
                      << std::fixed << std::setprecision(12) << std::setw(22)
                      << atom.position[1] << " " << std::fixed << std::setprecision(12) << std::setw(22) << atom.position[2] << "\n";
    });
    output_stream.close();
    std::cout << "Wrote to a grain file " << output_file_name+".voronoi" << " successfully." << std::endl;
}

void NanocrystalGenerator::save_params()
{
    std::cout << "Saving grains to a atomsk param file" << std::endl;
    std::ofstream output_stream(output_file_name+".params.txt", std::ios_base::out);

    output_stream << "# random generation results of nanocrystal generator" << std::endl;
    output_stream << "# the voronoi tesselation used seed " << seed << std::endl;

    output_stream << "box ";
    for (int i = 0; i<3; ++i) {
        output_stream << std::right << std::fixed << std::setw(16) << std::setprecision(6) << box.max()[i];
    }
    output_stream << "\n";
    std::for_each(grains.begin(),grains.end(),[&output_stream](auto &&grain){
        output_stream << "node ";
        for (int i = 0; i<3; ++i) {
            output_stream << std::right << std::fixed << std::setw(16) << std::setprecision(6) << grain.position[i];
        }
        for (int i = 0; i<3; ++i) {
            output_stream << std::right << std::fixed << std::setw(16) << std::setprecision(6) << grain.euler_angle[i];
        }
        output_stream << "\n";
    });
    std::cout << "params saved" << std::endl;

}

void NanocrystalGenerator::save_grain_statistics()
{
    std::cout << "Saving grain statistics to a file" << std::endl;
    std::ofstream output_stream(output_file_name+".grains", std::ios_base::out);

    output_stream << "#CSV for each of the grains\n#id;node_point;atom#;volume;mass_center_point;estimated_volume\n";

    for (auto grain = grains.begin(); grain!=grains.end();++grain) {
        output_stream << grain->grain_id << ';' <<std::fixed << std::setprecision(12)<< grain->position[0]<<','<<grain->position[1]<<','<<grain->position[2] << ';' << grain->atom_count << ';' << grain->volume << ';' << grain->mass_center_point[0]<<','<<grain->mass_center_point[1]<<','<<grain->mass_center_point[2] << ';' << grain->estim_volume<<"\n";
    }
    output_stream.close();

    std::cout << "Grain statistics saved" << std::endl;

}

bool NanocrystalGenerator::read_lattice(const std::string &filename)
{
    std::ifstream input_stream(filename, std::ios_base::in);

    std::string line = "";
    int number_of_lattice_atoms = 0;
    int number_of_atom_types = 0;
    for (int i = 0; i<2; ++i){
        std::getline(input_stream, line);
    }
    std::getline(input_stream, line);
    std::stringstream s1(line);
    s1 >> number_of_lattice_atoms;


    while (line.find("atom types") == std::string::npos) {
        std::getline(input_stream, line);
    }
    std::stringstream s2(line);
    s2 >> number_of_atom_types;

    // minimum and maximum to shift vectors
    while (line.find("xlo xhi") == std::string::npos) {
        std::getline(input_stream, line);
    }
    {
        double xmin = 0;
        double xmax = 0;
        std::stringstream s2(line);
        s2 >> xmin;
        s2 >> xmax;
        custom.shift_values[0]=(xmax-xmin);
    }
    while (line.find("ylo yhi") == std::string::npos) {
        std::getline(input_stream, line);
    }
    {
        double ymin = 0;
        double ymax = 0;
        std::stringstream s2(line);
        s2 >> ymin;
        s2 >> ymax;
        custom.shift_values[1]=(ymax-ymin);
    }
    while (line.find("zlo zhi") == std::string::npos) {
        std::getline(input_stream, line);
    }
    {
        double zmin = 0;
        double zmax = 0;
        std::stringstream s2(line);
        s2 >> zmin;
        s2 >> zmax;
        custom.shift_values[2]=(zmax-zmin);
    }


    while (line.find("Masses") == std::string::npos) {
        std::getline(input_stream, line);
    }
    // empty line
    std::getline(input_stream, line);
    // then all the masses for different atom types
    for (int i = 0; i < number_of_atom_types; ++i) {
        std::getline(input_stream, line);
        custom.masses.push_back(line);
    }

    // last but sure not least, the atoms in the lattice themselves
    while (line.find("Atoms") == std::string::npos) {
        std::getline(input_stream, line);
    }
    //empty line again
    std::getline(input_stream, line);

    for (int i = 0; i < number_of_lattice_atoms; ++i) {
        std::getline(input_stream, line);
        LmpAtom atom;
        std::stringstream s(line);
        int j=0;
        s >> j;
        s >> atom.type;
        s >> atom.coordinates[0];
        s >> atom.coordinates[1];
        s >> atom.coordinates[2];
        custom.atoms.push_back(atom);
    }
    std::cout << "Read a lattice file with "<< number_of_lattice_atoms << " atoms with " << number_of_atom_types << " different types" << std::endl;
    input_stream.close();
    return true;
}
