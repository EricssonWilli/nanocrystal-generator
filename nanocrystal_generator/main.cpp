#include <iostream>
#include "nanocrystalgenerator.hh"
#include <memory>
#include "CLI11.hpp"


int main(int argc, char** argv)
{
    std::unique_ptr<NanocrystalGenerator> generator = nullptr;
    try {
         generator = std::make_unique<NanocrystalGenerator>(argc, argv);
    } catch (const CLI::ParseError &e) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
